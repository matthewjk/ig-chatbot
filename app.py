from flask import Flask
from flask import request
from flask import render_template
import urllib.request, json, requests

app = Flask(__name__)

@app.route('/')
def resolve():
    code = request.args.get("code", None)
    if code == None or code == "":
        return render_template("error.html",text="Make sure to accept the authorization request!")
    headers = {'User-Agent': 'Mozilla/5.0'}
    url = "https://api.instagram.com/oauth/access_token"
    params = {"client_id": "db12089d9adf4be08571488c257bbe6b",
        "client_secret":"c7e38d9689f945c39e24ebbd88da44a9",
        "grant_type":"authorization_code",
        "redirect_uri":"https://ig-accountinfo.herokuapp.com/",
        "code":code}
    session = requests.Session()
    response = requests.post(url, headers=headers,data=params)
    if len(response.json())==3:
        return render_template("error.html",text="Something went wrong, invalid code")
    code = response.json()["access_token"]
    url = "https://api.instagram.com/v1/users/self/?access_token="+code
    weburl = urllib.request.urlopen(url)
    data = weburl.read()
    encoding = weburl.info().get_content_charset('utf-8')
    data=json.loads(data.decode(encoding))
    username = data["data"]["username"]
    profpic = data["data"]["profile_picture"]
    followers = data["data"]["counts"]["followed_by"]
    following = data["data"]["counts"]["follows"]
    postnum = data["data"]["counts"]["media"]
    name = data["data"]["full_name"]
    if name=="":
        name="You haven't set your name"
    bio = data["data"]["bio"]
    if bio=="":
        bio="You haven't set your bio"
    website = data["data"]["website"]
    if website=="":
        website="You haven't set your website"
    url = "https://api.instagram.com/v1/users/self/media/recent?access_token="+code
    weburl = urllib.request.urlopen(url)
    data = weburl.read()
    encoding = weburl.info().get_content_charset('utf-8')
    data=json.loads(data.decode(encoding))
    caption=data["data"][0]["caption"]["text"]
    likes=data["data"][0]["likes"]["count"]
    return(render_template("profile.html",likes=likes, caption=caption, username=username,profpic=profpic, postnum=postnum, followers=followers, following=following, name=name, bio=bio, website=website))

@app.errorhandler(404)
def page_not_found(e):
    return render_template("error.html",text="Error 404, nothing here")