[![Build Status](https://gitlab.com/matthewjk/ig-chatbot/badges/master/build.svg)](https://gitlab.com/matthewjk/ig-chatbot/pipelines/)
[![Line Count](https://tokei.rs/b1/gitlab/matthewjk/ig-chatbot?category=lines)](https://gitlab.com/matthewjk/ig-chatbot)

# IG Account Info

[![forthebadge](https://forthebadge.com/images/badges/check-it-out.svg)](https://api.instagram.com/oauth/authorize/?client_id=db12089d9adf4be08571488c257bbe6b&redirect_uri=https://ig-accountinfo.herokuapp.com/&response_type=code)

Click the button above to view the page, you will be prompted to authorise the api with your instagram account.


## Install

To setup initially and run locally run `./setup.sh`

To run once installed enter the virtual environment with `source venv/bin/activate`

Start the server with either `gunicorn app:app` or `python -m flask run`

## Requirements

- [Python3](https://www.python.org/download/releases/3.0/)

- [Flask](http://flask.pocoo.org/docs/1.0/#)

- [Requests](http://docs.python-requests.org/en/master/)