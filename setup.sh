#!/bin/bash 
ENV_NAME=$(pwd)
cd $ENV_NAME
echo "Creating python virtaul environment:"
python3 -m venv venv
source $ENV_NAME/venv/bin/activate
pip3 install -r requirements.txt
export FLASK_APP=app.py
echo "Started gunicorn server on port 8000"
gunicorn --access-logfile - --bind 0.0.0.0:8000 app:app
printf -- "\033[32m Use command 'deactivate', to exit virtual environment \033[0m\n";
printf -- "\033[32m Command does not need to be rerun each time \033[0m\n";
printf -- "\033[32m Enter virtual environment with 'source venv/bin/activate' and run 'gunicorn app:app \033[0m\n";
